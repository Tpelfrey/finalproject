package Homework14;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.imageio.ImageIO;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

public class llModel{

	private JsonElement SummonerInfo;
	private JsonElement ChampionInfo;
	private JsonElement MasteryInfo;
	
	static private String championID;
	static private String summonerID;
	static public String searchRegion = "NA1";
	public String errorMessage = null;
	// This MUST be replaced every 24 hours to keep it in working order
	private final String apiKey = 
			"RGAPI-1c27737e-c866-4a6d-bacd-7f0a369dc1fd";
	
	String getSummonerName(String summonerName)
	{
		// Acquires Summoner Name from Textboxes
		return summonerName;
	}
	
	String getAccountID()
	{
		// Acquires Account ID from :Summoners/V3:
		
		return SummonerInfo.getAsJsonObject().get("accountId")
				.getAsString();
	}
	
	String getSummonerID()
	{
		// Acquires SummonerID from :Summoners/V3:

		summonerID = SummonerInfo.getAsJsonObject()
				.get("id").getAsString();
		return summonerID;
	}
	
	String getChampionName(String championName)
	{
		// Acquires Account ID from :Static-Data-/V3: Local File (ChampionInfo.json)

				return ChampionInfo.getAsJsonObject().
						get("data").getAsJsonObject().
						get(championName).getAsJsonObject().
						get("name").getAsString();
				
	}
	
	@SuppressWarnings("static-access")
	String getChampionID(String championName)
	{
		// Acquires Account ID from :Static-Data-/V3: Local File (ChampionInfo.json)
		
		this.championID = ChampionInfo.getAsJsonObject().
				get("data").getAsJsonObject().
				get(championName).getAsJsonObject().
				get("id").getAsString();
		
		return championID;
	}
	
	public Image getChampionIcon(String championName)
	{
		Image championIcon = null;
		try 
		{
		URL llURL = new URL("http://ddragon.leagueoflegends.com/cdn/7.5.2/img/champion/" + championName + ".png");

	      // Open connection
	      InputStream is = llURL.openStream();
	      BufferedImage bi = ImageIO.read(is);
	      championIcon = SwingFXUtils.toFXImage(bi, null);
	      // Close Connection
	      is.close();
		}
		catch(IOException ioe)
		{
		}

		return championIcon;
	}
	
	public String getChampionLevel()
	{
		try
		{
		String championLevel = MasteryInfo.getAsJsonObject().get("championLevel").getAsString();
		return championLevel;
		}
		catch(NullPointerException npe)
		{
			errorMessage = "404 Error - Mastery Data Not Found";
			return "";
		}
	}
	
	public String getCurrentChampionXP() 
	{
			try
			{
				String currentChampionXP = MasteryInfo.getAsJsonObject().get("championPoints").getAsString();
				return currentChampionXP;
			}
			catch(NullPointerException npe)
			{
				return "";
			}
	}
	
	public String getNextLevelChampionXP() 
	{
			try
			{
				String NextLevelChampionXP = MasteryInfo.getAsJsonObject().get("championPointsUntilNextLevel").getAsString();
				return NextLevelChampionXP;
			}
			catch(NullPointerException npe)
			{
				return "";
			}
	}
	
	public String getChampionChest() 
	{
			try
			{
				String championChest = MasteryInfo.getAsJsonObject().get("chestGranted").getAsString();
				return championChest;
			}
			catch(NullPointerException npe)
			{
				return "";
			}
	}
	
	public boolean getSummonerChampionInfo(String summonerName, String championName, String searchRegion)
	  {
	    try
	    {
	    	// SEARCHES :SUMMONER/V3:
	      URL llURL = new URL("https://" + searchRegion + ".api.riotgames.com/lol/summoner/v3/summoners/by-name/"
	    		  + summonerName + "?api_key=" + apiKey);

	      // Open connection
	      InputStream is = llURL.openStream();
	      BufferedReader br = new BufferedReader(new InputStreamReader(is));

	      // Read the results into a JSON Element
	      SummonerInfo = new JsonParser().parse(br);
	      
	      // Close connection
	      is.close();
	      br.close();
	    }
	    catch (java.io.UnsupportedEncodingException uee)
	    {
	    }
	    catch (java.net.MalformedURLException mue)
	    {
	    }
	    catch (java.io.IOException ioe)
	    {
	    }
	    catch (java.lang.NullPointerException npe)
	    {
	    }
	    
	    try
	    {
	    	// SEARCHES CHAMPIONINFO
	    	URL llURL = llModel.class.getResource("ChampionInfo.json");
	    	File f = new File(llURL.getFile());
	    	BufferedReader br = new BufferedReader(new FileReader(f));
	    	
	      // Read the results into a JSON Element
	      ChampionInfo = new JsonParser().parse(br);

	      // Close connection
	      br.close();
	    }
	    catch (java.io.UnsupportedEncodingException uee)
	    {
	    }
	    catch (java.net.MalformedURLException mue)
	    {
	    }
	    catch (java.io.IOException ioe)
	    {
	    }
	    catch (java.lang.NullPointerException npe)
	    {
	    }
		
	    try 
	    {
	    	// SEARCHES CHAMPION-MASTERY-V3
	    	
	    	// PROGRAMMERS NOTE: Initially, I tried assigning the value for championID and summonerID through the getID methods
	    	// however, this didn't actually set the ids in the main class.
	    	
	    	championID = ChampionInfo.getAsJsonObject().
					get("data").getAsJsonObject().
					get(championName).getAsJsonObject().
					get("id").getAsString();
	    	
	    	summonerID = SummonerInfo.getAsJsonObject()
					.get("id").getAsString();
		      
	    	URL llURL = new URL("https://" + searchRegion + ".api.riotgames.com/lol/champion-mastery/v3/champion-masteries/by-summoner/" 
		    		+ summonerID + "/by-champion/" + championID + "?api_key=" + apiKey);
	    	
		      // Open connection
		      InputStream is = llURL.openStream();
		      BufferedReader br = new BufferedReader(new InputStreamReader(is));
		      // Read the results into a JSON Element
		      MasteryInfo = new JsonParser().parse(br);
		      
		      // Close connection
		      is.close();
		      br.close();
	    }
	    catch (java.io.UnsupportedEncodingException uee)
	    {
	    }
	    catch (java.net.MalformedURLException mue)
	    {
	    }
	    catch (java.io.IOException ioe)
	    {
	    }
	    catch (java.lang.NullPointerException npe)
	    {
	    }
		return this.isValid(championName, summonerName);
	  }
	
	public boolean isValid(String championName, String summonerName) 
	{
		boolean champion = this.championExists(championName);
		boolean summoner = this.summonerExists(summonerName);
		boolean mastery = this.masteryInfoExists(summonerName);
		
		if((champion == false) && (summoner == false)) {
			errorMessage = "404 Error - Summoner and Champion Data Not Found";
		}
		
		else if(champion == false)
		{
			errorMessage = "404 Error - Champion Data Not Found";
		}
		
		else if(summoner == false) 
		{
			errorMessage = "404 Error - Summoner Data Not Found";
		}
		
		else if(mastery == false)
		{
			errorMessage = "404 Error - Mastery Data Not Found";
		}
		
		if(this.apiKeyWorks() == false) 
		{
			errorMessage = "Your API Key is Out of Date";
		}
		
		if((champion == true) && (summoner == true) && (mastery == true)) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unused")
	public boolean masteryInfoExists(String championName)
	  {
	    // If the link is not valid we will get an error field in the JSON
	    try {
	    	
	    	URL llURL = new URL("https://" + searchRegion + ".api.riotgames.com/lol/champion-mastery/v3/champion-masteries/by-summoner/" 
		    		+ this.getSummonerID() + "/by-champion/" + this.getChampionID(championName) + "?api_key=" + apiKey);
	    	
		      // Open connection
		      InputStream is = llURL.openStream();
		      BufferedReader br = new BufferedReader(new InputStreamReader(is));
		      // Read the results into a JSON Element
		      MasteryInfo = new JsonParser().parse(br);
		      
		      // Close connection
		      is.close();
		      br.close();
	    	
	      String error = MasteryInfo.getAsJsonObject().get("status").
		  			 getAsJsonObject().get("status_code")
		  			 .getAsString();
	      return false;
	    }
	    catch (java.lang.NullPointerException npe)
	    {
	      // We did not see error so this is a valid URL
	      return true;
	    }
	    catch (java.net.MalformedURLException mue)
	    {
	    	return false;
	    }
	    catch (java.io.IOException ioe)
	    {
	    	return false;
	    }
	  }
	
	@SuppressWarnings("unused")
	public boolean summonerExists(String summonerName)
	{
		try
	    {
	    	// SEARCHES :SUMMONER/V3:
	      URL llURL = new URL("https://" + searchRegion + ".api.riotgames.com/lol/summoner/v3/summoners/by-name/"
	    		  + summonerName + "?api_key=" + apiKey);

	      // Open connection
	      InputStream is = llURL.openStream();
	      BufferedReader br = new BufferedReader(new InputStreamReader(is));

	      // Read the results into a JSON Element
	      SummonerInfo = new JsonParser().parse(br);
	      
	      // Close connection
	      is.close();
	      br.close();
			
		String notFound = SummonerInfo.getAsJsonObject().get("status").
	  			 getAsJsonObject().get("status_code")
	  			 .getAsString();
		
		return false;
		}
		catch (java.lang.NullPointerException npe)
		{
			// Data exists.
			return true;
		}
		catch (java.io.UnsupportedEncodingException uee)
	    {
			return false;
	    }
	    catch (java.net.MalformedURLException mue)
	    {
	    	return false;
	    }
	    catch (java.io.IOException ioe)
	    {
	    	return false;
	    }
	}
	
	@SuppressWarnings("unused")
	public boolean championExists(String championName)
	{
		 try
		    {
		    	// SEARCHES CHAMPIONINFO
		    	URL llURL = llModel.class.getResource("ChampionInfo.json");
		    	File f = new File(llURL.getFile());
		    	BufferedReader br = new BufferedReader(new FileReader(f));
		    	
		      // Read the results into a JSON Element
		      ChampionInfo = new JsonParser().parse(br);

		      // Close connection
		      br.close();
		      
		      String error = ChampionInfo.getAsJsonObject().
						get("data").getAsJsonObject().
						get(championName).getAsJsonObject().
						get("name").getAsString();
		      return true;
		      
		    }
		    catch (java.io.UnsupportedEncodingException uee)
		    {
		    	return false;
		    }
		    catch (java.net.MalformedURLException mue)
		    {
		    	return false;
		    }
		    catch (java.io.IOException ioe)
		    {
		    	return false;
		    }
		    catch (java.lang.NullPointerException npe)
		    {
		    	return false;
		    }
	}
	
	@SuppressWarnings("unused")
	public boolean apiKeyWorks()
	{
		try
	    {
	    	// SEARCHES :SUMMONER/V3:
	      URL llURL = new URL("https://" + searchRegion + ".api.riotgames.com/lol/status/v3/shard-data?api_key=" + apiKey);

	      // Open connection
	      InputStream is = llURL.openStream();
	      BufferedReader br = new BufferedReader(new InputStreamReader(is));

	      // Read the results into a JSON Element
	      JsonElement apiTester = new JsonParser().parse(br);
	      
	      // Close connection
	      is.close();
	      br.close();
			
		String notFound = apiTester.getAsJsonObject().get("name").getAsString();
		return true;
		}
		catch (java.lang.NullPointerException npe)
		{
			return false;
		}
		catch (java.io.UnsupportedEncodingException uee)
	    {
			return false;
	    }
	    catch (java.net.MalformedURLException mue)
	    {
	    	return false;
	    }
	    catch (java.io.IOException ioe)
	    {
	    	return false;
	    }
	}
	
	// TEST RELATED METHODS
	
}
