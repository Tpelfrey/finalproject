package Homework14;

import javafx.event.ActionEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class llController implements Initializable{

	 @FXML
	 private Text lblChampionID;
	 
	 @FXML
	 private Text lblChampionName;
	 
	 @FXML
	 private Text lblChampionLevel;
	 
	 @FXML
	 private Text lblChampionCurrentXP;
	 
	 @FXML
	 private Text lblChampionNextXP;
	 
	 @FXML
	 private Text lblChestAvailable;
	 
	 @FXML
	 private Text lblSummonerName;
	 
	 @FXML
	 private Text lblAccountID;
	 
	 @FXML
	 private Text lblSummonerID;
	 
	 @FXML
	 private Text lblErrorMessage;
	 
	 @FXML
	 private Text lblCurrentRegion;
	 
	 @FXML
	 private TextField txtChampName;
	 
	 @FXML
	 private TextField txtSummName;
	 
	 @FXML
	 private Button btnComSearch;
	 
	 @FXML
	 private ImageView imgChampionIcon;
	 
	 @FXML
	 private MenuItem RegionBR1;
	 
	 @FXML
	 private MenuItem ApplicationInformation;
	 
	 @FXML
	 private Button btnClose;
	 
	 @FXML
	 private void handleButtonClose()
	 {
		 Stage stage = (Stage) btnClose.getScene().getWindow();
		 stage.close();
	 }
	 
	 @FXML
	 private void handleHelpButton(ActionEvent e)
	 {
		 if (e.getSource() == ApplicationInformation)
		 {
			 try
			 {
	            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("llHelp.fxml"));
	            Parent root1 = (Parent) fxmlLoader.load();
	            Stage stage = new Stage();
	            stage.initModality(Modality.APPLICATION_MODAL);
	            stage.initStyle(StageStyle.UNDECORATED);
	            stage.setTitle("Help Window");
	            stage.setScene(new Scene(root1));  
	            stage.show();
	          }
			 catch(IOException ioe) 
			 {
				 
			 }
		 }
	 }
	 
	 @FXML
	 private void BR1()
	 {
		 llModel.searchRegion = "BR1";
		 	lblCurrentRegion.setText("Brazil");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionEUN1;
	 
	 @FXML
	 private void EUN1()
	 {
		 llModel.searchRegion = "EUN1";
		 	lblCurrentRegion.setText("Europe Nordic & East");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionEUW1;
	 
	 @FXML
	 private void EUW1()
	 {
		 llModel.searchRegion = "EUW1";
		 	lblCurrentRegion.setText("Europe West");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionJP1;
	 
	 @FXML
	 private void JP1()
	 {
		 llModel.searchRegion = "JP1";
		 	lblCurrentRegion.setText("Japan");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionLA1;
	 
	 @FXML
	 private void LA1()
	 {
		 llModel.searchRegion = "LA1";
		 	lblCurrentRegion.setText("Latin America North");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionLA2;
	 
	 @FXML
	 private void LA2()
	 {
		 llModel.searchRegion = "LA2";
		 	lblCurrentRegion.setText("Latin America South");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionNA1;

	 @FXML
	 private void NA1()
	 {
		 llModel.searchRegion = "NA1";
		 	lblCurrentRegion.setText("North America");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionOC1;
	 
	 @FXML
	 private void OC1()
	 {
		 llModel.searchRegion = "OC1";
		 	lblCurrentRegion.setText("Oceania");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionKR;
	 
	 @FXML
	 private void KR()
	 {
		 llModel.searchRegion = "KR";
		 	lblCurrentRegion.setText("Republic of Korea");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionRU;
	 
	 @FXML
	 private void RU()
	 {
		 llModel.searchRegion = "RU";
		 	lblCurrentRegion.setText("Russia");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private MenuItem RegionTR1;
	 
	 @FXML
	 private void TR1()
	 {
		 llModel.searchRegion = "TR1";
		 	lblCurrentRegion.setText("Turkey");
		 	lblErrorMessage.setText("");
			lblSummonerName.setText("");
			lblSummonerID.setText("");
			lblAccountID.setText("");
			lblChampionID.setText("");
			lblChampionName.setText("");
			lblChampionLevel.setText("");
			lblChampionCurrentXP.setText("");
			lblChampionNextXP.setText("");
			lblChestAvailable.setText("");
			imgChampionIcon.setImage(null);
	 }
	 
	 @FXML
	 private void handleButtonAction(ActionEvent e)
	 {
		 // Create object to access model
		 llModel llinfo = new llModel();
		 
		 //Check if Go button pressed
		 if (e.getSource() == btnComSearch)
		 { 
			 String summonerName = txtSummName.getText();
			 String championName = txtChampName.getText();
			 String searchRegion = llModel.searchRegion;
			 
				lblSummonerName.setText("");
				lblSummonerID.setText("");
				lblAccountID.setText("");
				lblChampionID.setText("");
				lblChampionName.setText("");
				lblChampionLevel.setText("");
				lblChampionCurrentXP.setText("");
				lblChampionNextXP.setText("");
				lblChestAvailable.setText("");
				imgChampionIcon.setImage(null);
			 
				if(llinfo.isValid(championName, summonerName) == true) 
			{	
				// Valid Successful
				if(llinfo.getSummonerChampionInfo(summonerName, championName, searchRegion) == true)
				{	
					lblSummonerName.setText(llinfo.getSummonerName(summonerName));
					lblSummonerID.setText(llinfo.getSummonerID());
					lblAccountID.setText(llinfo.getAccountID());
					lblChampionID.setText(llinfo.getChampionID(championName));
					lblChampionName.setText(llinfo.getChampionName(championName));
					imgChampionIcon.setImage(llinfo.getChampionIcon(championName));
					lblChampionLevel.setText(llinfo.getChampionLevel());
					lblChampionCurrentXP.setText(llinfo.getCurrentChampionXP());
					lblChampionNextXP.setText(llinfo.getNextLevelChampionXP());
					lblChestAvailable.setText(llinfo.getChampionChest());
					lblErrorMessage.setText(llinfo.errorMessage);
				}
				else if(llinfo.getSummonerChampionInfo(summonerName, championName, searchRegion) == false)
				{
					lblErrorMessage.setText(llinfo.errorMessage);
				}
			}
			else
			{
				// Valid Failed
				lblErrorMessage.setText(llinfo.errorMessage);
				lblSummonerName.setText("");
				lblSummonerID.setText("");
				lblAccountID.setText("");
				lblChampionID.setText("");
				lblChampionName.setText("");
				lblChampionLevel.setText("");
				lblChampionCurrentXP.setText("");
				lblChampionNextXP.setText("");
				lblChestAvailable.setText("");
				imgChampionIcon.setImage(null);
			}
		 }
	 }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}
}
