package Homework14;

import static org.junit.Assert.*;

import org.junit.Test;

public class llTest {

	@Test
	public void testGetAccountID()
	{
		// Rereading of the API led me to realize that AccountName wasn't the variable needed
		// It turns out that summonerName is the variable needed. Any references to account name
		// will be replaced with summonerName from here on.
		
		//This accounts id is 207762046, so we will assert that it equals that
		String summonerName = "TechMarineSolsti";
		
		llModel llInfo = new llModel();
	
		llInfo.summonerExists(summonerName);
		String AccID = llInfo.getAccountID();
		
		assertEquals("207762046",AccID);
	}
	
	@Test
	public void testGetSummonerID()
	{
		String summonerName = "TechMarineSolsti";
		
		llModel llInfo = new llModel();
		
		llInfo.summonerExists(summonerName);
		String sumID = llInfo.getSummonerID();
		
		assertEquals("46174771",sumID);
	}
	
	@Test
	public void testGetChampionID()
	{
		// We will be searching the champion Azir, who has a id of 268
		
				String championName = "Azir";
				
				llModel llInfo = new llModel();
				
				llInfo.championExists(championName);
				String champID = llInfo.getChampionID(championName);
				
				assertEquals("268",champID);
				
	}
	
	@Test
	public void testGetSummonerName()
	{
		String summonerName = "TechMarineSolsti";
		
		llModel llInfo = new llModel();
		
		String sumName = llInfo.getSummonerName(summonerName);
		assertEquals("TechMarineSolsti",sumName);
	}
	
	@Test
	public void testGetChampionName()
	{
		String championName = "Azir";
		
		llModel llInfo = new llModel();
		
		llInfo.championExists(championName);
		String champName = llInfo.getChampionName(championName);
		
		assertEquals("Azir",champName);
	}
	
	@Test
	public void testGetChampionLevel()
	{
		// Should come out to 5
		String summonerName = "TechMarineSolsti";
		String championName = "Azir";
		
		llModel llInfo = new llModel();
		
		llInfo.summonerExists(summonerName);
		llInfo.championExists(championName);
		llInfo.masteryInfoExists(championName);
		
		String champLevel = llInfo.getChampionLevel();
		
		assertEquals("5",champLevel);
	}
	
	@Test
	public void testGetCurrentXP()
	{
		// Should come out to 33597
				String summonerName = "TechMarineSolsti";
				String championName = "Azir";
				
				llModel llInfo = new llModel();
				
				llInfo.summonerExists(summonerName);
				llInfo.championExists(championName);
				llInfo.masteryInfoExists(championName);
				
				String champCurXP = llInfo.getCurrentChampionXP();
				
				assertEquals("33597",champCurXP);
	}
	
	@Test
	public void testGetNextLevelXP()
	{
		// Should come out to 0
		String summonerName = "TechMarineSolsti";
		String championName = "Azir";
		
		llModel llInfo = new llModel();
		
		llInfo.summonerExists(summonerName);
		llInfo.championExists(championName);
		llInfo.masteryInfoExists(championName);
		
		String champNEXP = llInfo.getNextLevelChampionXP();
		
		assertEquals("0",champNEXP);
	}
	
	@Test
	public void testGetChest()
	{
		// Should come out to 0
				String summonerName = "TechMarineSolsti";
				String championName = "Azir";
				
				llModel llInfo = new llModel();
				
				llInfo.summonerExists(summonerName);
				llInfo.championExists(championName);
				llInfo.masteryInfoExists(championName);
				
				String champChest = llInfo.getChampionChest();
				
				assertEquals("false",champChest);
	}
	
	@Test
	public void testInvalidAccount()
	{
		// Being that new accounts are made all the time, this test will have to
		// be reworked eventually
		String summonerName = "fesdzshhj";
		
		llModel llInfo = new llModel();
		
		boolean invalidAcc = llInfo.summonerExists(summonerName);
		
		assertEquals(false,invalidAcc);
	}
	
	@Test
	public void testInvalidCharacterName()
	{
		String championName = "NotACharacter";
		
		llModel llInfo = new llModel();
		
		Boolean invalidChamp = llInfo.championExists(championName);
		
		assertEquals(false,invalidChamp);
	}
}
